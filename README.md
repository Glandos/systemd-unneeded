systemd-unneeded
===

A lot of systemd socket activated units don't have a setting to auto shutdown after some idle time, like [iodine](https://manpages.debian.org/unstable/iodine/iodined.8.en.html#i). So once activated, they never shutdown.
This is possible to use [systemd-socket-proxyd](https://manpages.debian.org/unstable/systemd/systemd-socket-proxyd.8.en.html), but it requires to use an intermediate program to pass all packets. And sometimes, it also requires another socket.

systemd-unneeded list all matching units, and execute an action on it. Sane default option set makes it easy to quickly shutdown every socket activated units:


## Example
```
systemd-unneeded --name 'php-fpm*.service' --max-tasks 1 --status "Processes active: 0"
```
This will stops all PHP-FPM units with at most one active task (the master process), and which reported status (using `sd_notify`) says that there is no currently active process.
