#!/usr/bin/env python3

from pystemd.systemd1 import Manager, Unit
import click
import re
from typing import Optional
import pprint


@click.command()
@click.option(
    "-n",
    "--name",
    default=["*.service"],
    multiple=True,
    help="Match unit name. Supports globbing.",
)
@click.option(
    "-s",
    "--status",
    type=re.compile,
    help="Match process status as reported using sd_notify. This is a regular expression.",
)
@click.option(
    "-S",
    "--state",
    default=["active"],
    multiple=True,
    help="Match unit current state. Default to active units.",
)
@click.option(
    "-a",
    "--action",
    default="stop",
    help="Action to run on match. Default to stop unit.",
)
@click.option(
    "-t",
    "--triggered",
    default=True,
    is_flag=True,
    help="Match unit that are triggered by another one. Useful for socket or path activated units.",
)
@click.option(
    "--max-tasks", type=int, help="Match unit with at most 'n' running tasks."
)
@click.option("--dry-run", is_flag=True, help="Don't run actions")
def load_services(
    name: list[str],
    status: Optional[re.Pattern],
    state: list[str],
    action: str,
    max_tasks: Optional[int],
    triggered: bool,
    dry_run: bool,
):
    manager = Manager()
    manager.load()

    for (name, _, _, state, _, _, _, _, _, _) in manager.Manager.ListUnitsByPatterns(
        state, name
    ):
        unit = Unit(name, _autoload=True)

        match_tasks = max_tasks is None or unit.Service.TasksCurrent <= max_tasks
        if not match_tasks:
            continue

        match_socket = triggered and len(unit.Unit.TriggeredBy) > 0
        if not match_socket:
            continue

        match_status = status is None or status.search(unit.Service.StatusText.decode())
        if not match_status:
            continue

        click.echo(f"Unit {name.decode()} maching, executing {action} action")
        if not dry_run:
            unit.Unit.Stop(b"replace")


if __name__ == "__main__":
    load_services()
